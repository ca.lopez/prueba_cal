
Nombre: Carlos Andres Lopez
correo-e: asraumbra@gmail.com

Instrucciones.

1) Pasos previos.

Usar motor de base de datos PostgreSQL.

Se debe crear usuario en la base de datos, ademas de habilitar los permisos de acceso y operacion de insercion, para este ejemplo se configuro el usuario:

  user: usr_pragma
  pw: pragma321

Esta informacion debe ser adicionada al archivo config.yml en la carpeta PipeLib, es necesario para operar sobre la base de datos

2) Crear base de datos y tabla.

En la carpeta scripts se encuentra:

create_db.sql
create_table.sql
queries.sql

Se debe ejecutar create_db.sql para crear la base de datos, luego create_table.sql para crear la tabla.

3) COnfiguracion de origen de datos.

al archivo Utils.py en la carpeta PipeLib se debe agregar la informacion de los directorios de trabajo.

La variable path_dir debe contener el directorio donde reposan los archivos a migrar.

La variable path_config debe contener la direccion del archivo de configuracion ajustado al arbol de archivos de la maquina donde se corre la aplicacion.

4) Ejecucion.

Para ejecutar el aplicativo escribir ubicado en la carpeta donde se descargó el aplicativo:

python3 App_carga.py 
