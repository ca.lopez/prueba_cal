import PipeLib as plib
import os
import pandas as pd

if __name__ == '__main__':
    origen = plib.entorno['path_dir']
    files = os.listdir(origen)
    excl = 'validation.csv'
    files.remove('validation.csv')
    print (files)

    stds = []
    for f in files:
        info_dc = plib.Load_file(f)
        info_dc['file'] = f
        stds.append(info_dc)


    #print(stds)
    dt_res = pd.DataFrame(stds)

    print('Antes de archivo validation.csv')
    print(dt_res)
    print('Cantidad total: ',dt_res['cant'].sum())
    print('Maximo valor: ',dt_res['vmax'].max())
    print('Minimo valor: ',dt_res['vmin'].min())
    print('Promedio: ',dt_res['vprom'].mean())

    info_dc = plib.Load_file(excl)
    info_dc['file'] = excl
    stds.append(info_dc)

    #print(stds)
    dt_res = pd.DataFrame(stds)

    print('Despues de archivo validation.csv')
    print(dt_res)
    print('Cantidad total: ',dt_res['cant'].sum())
    print('Maximo valor: ',dt_res['vmax'].max())
    print('Minimo valor: ',dt_res['vmin'].min())
    print('Promedio: ',dt_res['vprom'].mean())
