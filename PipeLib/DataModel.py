'''
Autor: Carlos Andres Lopez
correo-e: asraumbra@gmail.com

'''
from .db import *
import datetime
from sqlalchemy import Column, Integer, DateTime

class Price(Base):
    __tablename__ = 'prices'

    id = Column(Integer, primary_key=True)
    date_price = Column(DateTime, default=datetime.datetime.utcnow)
    price = Column(Integer)
    user_id = Column(Integer)


    def __init__(self, date_price, price, user_id):
        self.price = price
        self.user_id = user_id
        self.date_price = date_price
