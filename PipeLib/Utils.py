'''
Autor: Carlos Andres Lopez
correo-e: asraumbra@gmail.com

'''
import pandas as pd
import csv
from statistics import mean
import PipeLib as plib

entorno = {
            'path_dir': '/home/andres/Documentos/hv/Pragma/Repo_data/dataPruebaDataEngineer/',
            'path_config':'/home/andres/Documentos/hv/Pragma/prueba_cal/PipeLib/config.yml',
          }

def Load_file (path_file):
    ls_price = []

    file_name = entorno['path_dir'] + path_file
    with open(file_name, newline='') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=',')
        for row in reader:
            if len(row['price']) > 0:
                fsep = row['timestamp'].split('/')
                fsep[0], fsep[1] = fsep[1], fsep[0]
                fecha = '/'.join(fsep)
                p = plib.Price(fecha, row['price'], row['user_id'])
                plib.session.add(p)
                plib.session.commit()
                ls_price.append(int(row['price']))
            info_datos = f"{row['timestamp']} {row['price']} {row['user_id']}"
            info_carga = f" cant:{len(ls_price)} max:{max(ls_price)}" \
                         f" min:{min(ls_price)} prom:{mean(ls_price)}"
            print(info_datos + " - " + info_carga)
    return {'cant':len(ls_price), 'vmax':max(ls_price), 'vmin':min(ls_price), 'vprom':mean(ls_price)}
