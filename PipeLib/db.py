'''
Autor: Carlos Andres Lopez
correo-e: asraumbra@gmail.com

'''
from .Utils import *
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import yaml

path_conf = entorno['path_config']
conf = yaml.safe_load(open(path_conf))
user = conf['credentials']['user']
pw = conf['credentials']['pw']
db = "pragma_db"

engine = create_engine("postgresql+psycopg2://"+user+":"+pw+"@localhost/"+db)
Session = sessionmaker(bind=engine)
session = Session()

Base = declarative_base()
